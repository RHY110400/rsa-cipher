#include "rsa.h"

using namespace std;

int main() {

  cout << "===================================" << endl;
  cout << "    Rob's Home Brewed RSA Tool" << endl;
  cout << "===================================" << endl << endl;

  cout << "Use for encoding and decoding doubles" << endl;
  cout << "Works best for numbers in range 0-512" << endl << endl;


   // create the rsa opject
   RSA rsa = RSA();

   // create the keys using the in built function
   double *key_pair;
   key_pair = NULL;

   //main loop
   char choice;
   double message;


   while (true){
     cout << "Usage : 1) generate keys  2) encode message  3) decode message  x) exit" << endl;
     cin >> choice;

     switch (choice){
       case '1':
         if (key_pair != NULL){
           free(key_pair);
         }
        cout <<  "[*] Generating keys" << endl;
        key_pair = rsa.generate_pair();
       break;
       case '2':
        if (key_pair == NULL){
          cout << "Please generate keys" << endl;
          break;
        }
        cin >> message;
        cout << rsa.encode(message, key_pair[1], key_pair[2]) << endl;
      break;
      case '3':
        if (key_pair == NULL){
          cout << "Please generate keys" << endl;
          continue;
        }
        cin >> message;
        cout << rsa.decode(message, key_pair[0], key_pair[2]) << endl;
      break;
      case 'x':
        cout << "ThankYou for using my tool :)" << endl;
        exit(0);
      break;
      default:
        cout << "No command given" << endl;
      break;
     }


     // clear the buffer
     cin.clear();

   }



   //finally free the allocated memory
   free(key_pair);

   return 0;
}
