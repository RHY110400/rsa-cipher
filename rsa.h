#include<iostream>
#include<math.h>

#include <stdlib.h>
#include <time.h>

/*
An implementaion of the functions required to use the RSA cipher

Keys are generated using the generate_pair method

Then messages will be coded and decoded using the relevant functions

@ note this class does not store the keys as this is not the intent of it, the intent
is for the class to be able to be a utility for other programs and for it to allow
modular programming
*/
class RSA{
  int gcd(int, int);
  bool check_prime(int);
  int generate_prime(int, int);
public:
  double * generate_pair();
  double decode(double c, double d, double n);
  double encode(double m, double e, double n);

};
