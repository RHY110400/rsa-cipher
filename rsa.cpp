#include "rsa.h"

using namespace std;

/*
Function to find the greatest common divisor between two numbers

@param a, b the two numbers from which the gcd will be found

@ return the gcd as an int
*/
int RSA::gcd(int a, int b) {
   int t;
   while(1) {
      t= a%b;
      if(t==0)
         return b;
      a = b;
      b= t;
   }
}

/*
checks if a number passed to it is infact check_prime

@params n is the number which will be checked

@ returns a boolean for whether the argument is prime or not
*/
bool RSA::check_prime(int n){
  int i, m=0;
  m=n/2;
  for( i = 2; i <= m; i++)
      if(n % i == 0)
          return false;
  return true;
}

/*
Function creates a random prime number which will far between the min and the max

@ params min, max are the ranges given which the number should fall between

@returns an int of a random generate prime
*/
int RSA::generate_prime(int min, int max){
  /* initialize random seed: */
  srand (time(NULL));
  int i;
  while (true){
    i = rand() % (max) + min;
    if (check_prime(i))
      return i;
  }
}

/*
This function will generate a key pair and the modulo n which can then be used for decrytion and encryption

@return a pointer to an array of 3 doubles with the encryption, decryption and modulo keys

@ note the return value must be freed to avoid memory leak
*/
double * RSA::generate_pair(){



   // create 2 random prime numbers
   double p = generate_prime(10, 50);
   double q = generate_prime(10, 50);

   //calculate n
   double n=p*q;
   double track;

   //calculate phi
   double phi= (p-1)*(q-1);


   //public key e for encrypt
   double e=10;

   //for checking that 1 < e < phi(n) and gcd(e, phi(n)) = 1; i.e., e and phi(n) are coprime.
   while(e<phi) {
      track = gcd(e,phi);
      if(track==1)
         break;
      else
         e++;
   }

   //generate private key d for decrypt
   //choosing d such that it satisfies d*e = 1 mod phi
   double d1=1/e;
   double d=fmod(d1,phi);

   // assign memory to a pointer for 3 doubles
   double *key_pair = (double*) calloc (3,sizeof(double));

   // assign the values to the array
   key_pair[0] = e;
   key_pair[1] = d;
   key_pair[2] = n;

   // make sure this is freed
   return key_pair;
}


/*
Function which encodes a given message with a key

@ param m is the plaintext message, e is the encryption key, n in the modulo

@ return a double of the next ciphertext message
*/
double RSA::encode(double m, double e, double n){

  // using maths library for power and fmod function, more accurate options
  double c = pow(m,e);
  c = fmod(c, n);

  return c;
}


/*
Function which decodes a ciphertext message with the dycryption key and modulo

@ param c is the ciphertext double, d is the decryption key as a double & n is the modulo as a double

@ return the plaintext message as a double
*/
double RSA::decode(double c, double d, double n){

  // using maths library for power and fmod function, more accurate options
  double m = pow(c, d);
  m = fmod(m, n);

  return m;
}
